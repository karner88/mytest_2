<?php
require_once('./controllers/UserController.php');
require_once('./model/UserModel.php');

if(@$_SERVER['REQUEST_URI'] =='/'){
    header("Location:view/login.php");
}
@$option = $_REQUEST['oplog'];

$user_controller = new UserController();

switch ($option) {
    case 'Login':
        $username = $_POST['user'];
        $password = $_POST['pass'];

        if($user_controller->login($username,$password))
        {
            header("Location:view/main.php");
        }else{
            header("Location:view/login.php?err=1");
        }
    break;

    case 'logout':
        $user_controller->logout();
        header("Location:view/login.php");
    break;

    case 'Register':
        if(isset($_POST['user']) && isset($_POST['usersurname']) && isset($_POST['email']) && isset($_POST['pass']))
        {
             if(strlen($_POST['pass']) < 5)
             {
                 header("Location:view/register.php?err=3");
             }else {

                 if($user_controller->register($_POST['user'],$_POST['usersurname'],$_POST['email'],$_POST['pass']))
                 {
                     header("Location:view/register.php?suc=1");
                 }else{
                     header("Location:view/register.php?err=4");
                 }
             }

        }else{
            header("Location:view/register.php?err=2");
        }
    break;
}
if(isset($_SERVER['REQUEST_URI']))
{
    $url = explode('/',$_SERVER['REQUEST_URI']);

    if(@$url[2]=='addData')
    {
        $addata = new $url[1];

        if($addata->{$url[2]}($_POST['postid'],$_POST['posts']))
        {
            echo '<div class="alert alert-succes">POST Added</div>';
        }
    }
    if(@$url[2]=='fetchData')
    {
        $fetchdata = new $url[1];
        $data=$fetchdata->{$url[2]}($_POST['postid']);

        if($data)
        {
             echo $data;
        }
    }
    if(@$url[2]=='delData')
    {
        $fetchdata = new $url[1];
        $data=$fetchdata->{$url[2]}($_POST['postid']);

        if($data)
        {
            echo '<div class="alert alert-succes">POST Deleted</div>';
        }
    }
}

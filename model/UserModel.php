<?php

require_once(__DIR__.'/../Config.php');

class UserModel  {
    private $username;
    private $database;

    public function __construct()
    {

    }
    public function connect()
    {
        $servername = Config::LOCALHOST;
        $usersname = Config::USER_NAME;
        $password = Config::DATA_PASS;

        $this->database = new PDO ("mysql:host=$servername", $usersname, $password);
        $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    public function createUser($username)
    {
        $this->username = $username;
    }
    public function get_username()
    {
        return $this->username;
    }
    public function set_username($username)
    {
        $this->username = $username;
    }
    public function addTables()
    {
        try {
            $sql = "CREATE DATABASE IF NOT EXISTS app ";
            $this->database->exec($sql);
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        $this->database->query("use app");

        $sql = "CREATE TABLE IF NOT EXISTS users (
          id INT(11)   AUTO_INCREMENT PRIMARY KEY,
           username VARCHAR(250) NOT NULL UNIQUE,
           usersurname VARCHAR(250) NOT NULL,
           email VARCHAR(250) NOT NULL,
           password VARCHAR(250) NOT NULL
           )";

        $this->database->exec($sql);

        $sql = "CREATE TABLE IF NOT EXISTS posts (
          id INT(11)   AUTO_INCREMENT PRIMARY KEY,
           userid VARCHAR(250) NOT NULL,
           posts VARCHAR(250) NOT NULL
          
           )";
        $this->database->exec($sql);
    }

    public function  register($username,$usersurname,$email,$pass)
    {
        $this->connect();
        try {
            $this->addTables();

            $statement=$this->database->prepare( "INSERT INTO  users(username,usersurname,email,password) VALUES (?,?,?,?)");
            if($statement->execute([$username,$usersurname,$email,$pass]))
            {
                return true;
            }

        } catch (PDOException $e) {
            return false;
        }
    }
    public function  checkLogin($username,$pass)
    {
        $this->connect();
        try {
            $this->addTables();

            $statement=$this->database->prepare("SELECT * FROM users WHERE (username=? AND password=?)");
            $statement->execute([$username,$pass]);
             if($statement->fetchAll())
            {
                return true;
            }
        } catch (PDOException $e) {
            echo  "<br>" . $e->getMessage();
        }
    }
    public function  addPost($postid,$posts)
    {
        $this->connect();
        try {
            $this->addTables();

            $statement=$this->database->prepare( "INSERT INTO  posts(userid,posts) VALUES(?,?)");
            if($statement->execute([$postid,$posts]))
            {
                return true;
            }
        } catch (PDOException $e) {
            echo  "<br>" . $e->getMessage();
        }
    }
    public function fetchPosts($postid)
    {
        $this->connect();
        try {
            $this->addTables();

            $statement = $this->database->prepare("SELECT * FROM  posts WHERE (userid=?) ORDER by id DESC ");

            $statement->execute([$postid]);
            if($statement)
            {
                $data=json_encode($statement->fetchAll());
              return  $data;
            }
        }catch (PDOException $e) {
            echo  "<br>" . $e->getMessage();
        }
    }
    public function delPosts($postid)
    {
        $this->connect();
        try {
            $this->addTables();

            $statement = $this->database->prepare("DELETE FROM  posts WHERE (id=?)");

            $statement->execute([$postid]);
            return true;

        }catch (PDOException $e) {
            echo  "<br>" . $e->getMessage();
        }
    }
    public function userlist()
    {
        $this->connect();
        try {
            $this->addTables();

            $statement = $this->database->prepare("SELECT username FROM users ORDER BY id");

            $statement->execute();
          return  $statement->fetchAll(PDO::FETCH_COLUMN);

        }catch (PDOException $e) {
            echo  "<br>" . $e->getMessage();
        }
    }
}
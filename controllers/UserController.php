<?php

class UserController {

    public function construct()
    {

    }
    public function login($username,$password)
    {
        if($this->authenticate($username,$password))
        {
            session_start();

            $user = new UserModel();
            $user->createUser($username);
            $_SESSION['user'] = $user;
            return true;
        } else{
            return false;
        }
    }
    public static function authenticate($u,$p)
    {
        $authentic = false;
          $check = new UserModel();
            $p=md5($p);
        if($check->checkLogin($u,$p))
        {

            $authentic = true;
            return $authentic;
        }
    }
    public function  logout()
    {
        session_start();
        session_destroy();
    }
    public function  register($username,$usersurname,$email,$pass)
    {
         $pass=md5($pass);
         $reguser = new UserModel();
        if($reguser->register($username,$usersurname,$email,$pass))
        {
         return true;
        }else{
            return false;
        }
    }
    public function  addData($postid,$posts) {
         $adddata = new UserModel();
        if($adddata->addPost($postid,$posts))
        {
            return true;
        }
    }
    public function  fetchData($postid)
    {
        $fetchdata = new UserModel();
        $fetch=$fetchdata->fetchPosts($postid);
        if($fetch)
        {
            return $fetch;
        }
    }
    public function  delData($postid)
    {
        $fetchdata = new UserModel();
         $fetchdata->delPosts($postid);
           return true;
    }
    public function userList()
    {
        $userlist = new UserModel();
       return $res=$userlist->userlist();
    }
}
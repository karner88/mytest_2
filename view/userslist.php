<?php
require_once('../controllers/UserController.php');
require_once('../model/UserModel.php');

session_start();
if(!isset($_SESSION['user']))
    {
        header("Location:login.php");
    }
$user = $_SESSION['user'];

$userlist = new UserController();
$userlist=$userlist->userList();

require_once('./layouts/userslist.html');
